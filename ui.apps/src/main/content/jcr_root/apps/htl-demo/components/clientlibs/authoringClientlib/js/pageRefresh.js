(function ($, channel) {
    'use strict';
    $(function () {
        channel.on('cq-layer-activated', function (event) {

            var pageUrl = window.location.href;
            var ANNOTATE_LAYER = 'Annotate';
            if ( event.prevLayer && event.layer !== event.prevLayer
                        && event.layer != ANNOTATE_LAYER  && event.prevLayer != ANNOTATE_LAYER ) {
                    location.reload();
                }
        });
    });
})(Granite.$, jQuery(document));
