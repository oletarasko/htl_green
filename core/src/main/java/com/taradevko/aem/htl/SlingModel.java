package com.taradevko.aem.htl;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

@Model(adaptables = SlingHttpServletRequest.class)
public class SlingModel implements TestBean {

	@Self
	private SlingHttpServletRequest request;

	private String parameterValue;

	@PostConstruct
	protected void init() {
		parameterValue = request.getParameter("param");
	}

	@Override
	public String getValue() {
		return parameterValue;
	}
}
