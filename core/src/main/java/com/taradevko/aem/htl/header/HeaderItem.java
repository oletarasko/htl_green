package com.taradevko.aem.htl.header;

public class HeaderItem {

	private String name;
	private String href;
	private boolean hidden;

	public HeaderItem(final String name, final String href, final boolean hidden) {
		this.name = name;
		this.href = href;
		this.hidden = hidden;
	}

	public String getName() {
		return name;
	}

	public String getHref() {
		return href;
	}

	public boolean isHidden() {
		return hidden;
	}
}
