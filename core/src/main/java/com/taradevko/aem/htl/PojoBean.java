package com.taradevko.aem.htl;

public class PojoBean implements TestBean {

	@Override
	public String getValue() {
		return "Hello I'm Pojo and there is no easy way for me to get parameter value.";
	}
}
