package com.taradevko.aem.htl.header;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = SlingHttpServletRequest.class)
public class HeaderModel {

	public Map<String, List<HeaderItem>> getItems() {
		TreeMap<String, List<HeaderItem>> items = new TreeMap<>();
		items.put("Canada", Arrays.asList(
				new HeaderItem("EN", "/content/we-retail/ca/en", false),
				new HeaderItem("FR", "/content/we-retail/ca/fr", true)));
		items.put("France",
				Collections.singletonList(new HeaderItem("FR", "/content/we-retail/fr/fr", false)));
		return items;
	}
}
