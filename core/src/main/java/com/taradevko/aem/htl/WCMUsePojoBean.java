package com.taradevko.aem.htl;

import com.adobe.cq.sightly.WCMUsePojo;

public class WCMUsePojoBean extends WCMUsePojo implements TestBean {

	private String parameterValue;

	@Override
	public void activate() throws Exception {
		parameterValue = getRequest().getParameter("param");
	}

	@Override
	public String getValue() {
		return parameterValue;
	}
}
